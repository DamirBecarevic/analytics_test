{{ config(materialized='view') }}

with total_sales as (
    select partner, currency, sum(face_value) as total_face_value
    from {{ ref('recent_transactions') }}
    group by partner, currency
),

partner_link as (
    select id, name 
    from mysql_source.partners
)

select pl.name as partner_name,
       ts.currency as currency,
       ts.total_face_value as total_face_value
from partner_link pl inner join total_sales ts on pl.id = ts.partner
order by ts.total_face_value desc, pl.name asc, ts.currency asc