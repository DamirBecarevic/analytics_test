{{ config(materialized='table') }}

with total_sales as (
    select retailer, currency, sum(face_value) as total_face_value
    from {{ ref('recent_transactions') }}
    group by retailer, currency
),

retailer_link as (
    select id, name 
    from mysql_source.retailers
)

select rl.name as retailer_name,
       ts.currency as currency,
       ts.total_face_value as total_face_value
from retailer_link rl inner join total_sales ts on rl.id = ts.retailer
order by ts.total_face_value desc, rl.name asc, ts.currency asc
