{{ config(materialized='ephemeral') }}

select 
    transaction_id,
    partner, 
    retailer, 
    currency, 
    face_value, 
    sales_date 
from mysql_source.transactions 
where to_timestamp(sales_date, 'YYYY-MM-DD HH24:MI:SS') > now() - interval '{{ var("months_before", "1") }} month'